<?php

namespace NicoSystem\Interfaces;

/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/30/2016
 * Time: 10:49 PM
 */
interface BasicCrudInterface
{
    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function getById($id, array $attributes = []);

    /**
     * @param array $params
     * @param bool $paginate
     * @param array $attributes
     * @return mixed
     */
    public function getList(array $params = [], $paginate = true, array $attributes = []);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function destroy($id, array $attributes = []);

    /**
     * @param array $inputs
     * @return mixed
     */
    public function create(array $inputs);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, array $attributes);
}
