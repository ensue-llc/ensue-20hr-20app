<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/30/2016
 * Time: 11:48 PM
 */

namespace NicoSystem\Requests;

use Illuminate\Foundation\Http\FormRequest;
use NicoSystem\Foundation\NicoResponseTraits;

/**
 * Class NicoRequest
 * @package NicoSystem\Requests
 */
class NicoRequest extends FormRequest
{
    use NicoResponseTraits;

    public function authorize(){
        return true;
    }

    public function rules(){
        return [];
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->responseValidationError($errors);
    }


}
