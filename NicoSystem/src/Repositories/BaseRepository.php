<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/30/2016
 * Time: 11:07 PM
 */

namespace NicoSystem\Repositories;

use App\System\AppBaseModel;
use App\System\AppConstants;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Exceptions\NicoException;
use NicoSystem\Filters\BaseFilter;
use NicoSystem\Foundation\Database\BaseModel;
use NicoSystem\Foundation\Status;
use NicoSystem\Interfaces\BasicCrudInterface;

abstract class BaseRepository implements BasicCrudInterface
{
    /**
     * @var BaseModel
     */
    protected $model = null;

    protected array $events = [];

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $params
     * @param bool $paginate
     * @param array $attributes
     * @return LengthAwarePaginator|Collection
     */
    public function getList(array $params = [], $paginate = true, array $attributes = [])
    {
        $builder = $this->attachOrderByQuery($params);
        $filter = $this->getFilter($builder);

        if ($filter) {
            $filter->attachFilterQuery($params);
        }

        if (array_get($params, 'all') == 1) {
            $paginate = false;
        }

        $this->onBeforeResult($builder);

        if ($attributes) {
            $builder->select($attributes);
        }

        if ($paginate) {
            return $builder->paginate();
        }

        return $builder->get();
    }

    /**
     * @param array $params
     * @return Builder
     */
    protected function attachOrderByQuery($params = []): Builder
    {
        $builder = $this->getQuery();
        $orderColumn = array_get($params, 'sort_by', $this->model->defaultSortColumn());
        $orderBy = array_get($params, 'sort_order', $this->model->defaultSortOrder());

        if (!in_array($orderColumn, $this->model->sortableColumns())) {
            $orderColumn = $this->model->defaultSortColumn();
        }

        $orderColumn = $this->model->mapSortKey($orderColumn);

        if (!$orderBy || !in_array($orderBy, ['asc', 'desc'])) {
            $orderBy = $this->model->defaultSortOrder();
        }

        if ($orderColumn) {

            $builder->orderBy($orderColumn, $orderBy);
        }
        return $builder;
    }

    protected function getQuery()
    {
        return $this->model->newQuery();
    }

    /**
     * @param Builder $builder
     * @return BaseFilter
     */
    public abstract function getFilter(Builder $builder);

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder)
    {
    }

    /**
     * @param $id
     * @param array $options
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id, array $options = [])
    {
        $model = $this->getQuery()->findOrFail($id);
        $this->dispatchEvent('deleting', $model);
        $model->delete();
        $this->dispatchEvent('deleted', $model);
        return true;
    }

    /**
     * @param $eventName
     * @param $model
     */
    protected function dispatchEvent($eventName, $model)
    {
        if (array_key_exists($eventName, $this->events)) {
            Event::dispatch(new $this->events[$eventName]($model));
        }
    }

    /**
     * @param array $inputs
     * @return mixed
     */
    public function create(array $inputs)
    {
        return $this->save($inputs);
    }

    /**
     * @param array $attributes
     * @param null $id
     * @return mixed
     */
    protected function save(array $attributes, $id = null)
    {
        if ($id) {
            $model = $this->getQuery()->findOrFail($id);
            if ($model->status === Status::STATUS_SUSPENDED) {
                throw new NicoBadRequestException("Resource is not editable", AppConstants::ERR_SUSPENDED_MODEL_NOT_EDITABLE);
            }
        } else {
            $model = $this->model->newInstance();
        }
        $model->fill($attributes);
        if ($id) {
            $this->dispatchEvent('updating', $model);
        } else {
            $this->dispatchEvent('creating', $model);
        }
        $model->save();
        if ($id) {
            $this->dispatchEvent('updated', $model);
        } else {
            $this->dispatchEvent('created', $model);
        }
        return $model;
    }

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, array $attributes)
    {
        return $this->save($attributes, $id);
    }

    /**
     * @param $id
     * @return AppBaseModel|BaseModel
     */
    public function togglePublish($id)
    {
        if (is_numeric($id)) {
            $model = $this->getById($id);
        } elseif ($id instanceof AppBaseModel) {
            $model = $id;
        } else {
            throw new NicoException("", 500);
        }
        if ($model->status == Status::STATUS_UNPUBLISHED) {
            $model->status = Status::STATUS_PUBLISHED;
        } elseif ($model->status == Status::STATUS_PUBLISHED) {
            $model->status = Status::STATUS_UNPUBLISHED;
        } elseif ($model->status == Status::STATUS_SUSPENDED) {
            throw new NicoBadRequestException("Cannot modify the model because the status is suspended", AppConstants::ERR_SUSPENDED_MODEL_NOT_EDITABLE);
        }
        $this->dispatchEvent('updating', $model);
        $model->save();
        $this->dispatchEvent('updated', $model);
        return $model;
    }

    /**
     * @param $id
     * @param array $attributes
     * @return BaseModel
     */
    public function getById($id, array $attributes = []): BaseModel
    {
        return $this->getQuery()->findOrFail($id);
    }

    /**
     * @param $id
     * @param $field
     * @param $value
     * @return mixed
     */
    protected function updateSingle($id, $field, $value)
    {
        $model = $this->getQuery()->findOrFail($id);
        $model->$field = $value;
        $this->dispatchEvent('updating', $model);
        $model->save();
        $this->dispatchEvent('updated', $model);
        return $model;
    }

}
