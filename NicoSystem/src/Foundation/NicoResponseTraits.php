<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/31/2016
 * Time: 12:23 AM
 */

namespace NicoSystem\Foundation;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Trait NicoResponseTraits
 * @package NicoSystem\Foundation
 */
trait NicoResponseTraits
{
    protected bool $api = true;

    /**
     * @param $body
     * @param string $codeText
     * @param string $messageCode
     * @param array $headers
     * @return JsonResponse
     */
    public function responseOk($body, $codeText = 'ok', $messageCode = 'ok', $headers = []): JsonResponse
    {
        return $this->nicoResponse($body, Response::HTTP_OK, $codeText, $messageCode, $headers);
    }

    /**
     * @param $body
     * @param int $status
     * @param string $codeText
     * @param string $messageCode
     * @param array $headers
     * @return JsonResponse
     */
    public function nicoResponse($body, $status = Response::HTTP_OK, $codeText = 'OK', $messageCode = 'ok', $headers = []): JsonResponse
    {
        if ($body instanceof Collection) {
            $body = new Paginator($body->all(), $body->count());
        }
        $status = $this->validateStatusCode($status);

        return response()->json([
            'body' => $body,
            'status' => [
                "message" => trans("appconstant." . $messageCode),
                'code' => $messageCode,
                'code_text' => $codeText,
            ]
        ], $status)->withHeaders($headers);
    }

    /**
     * @param int $code
     * @return int
     */
    public function validateStatusCode($code = 0): int
    {
        $status_code = array(100, 101, 102, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226,
            300, 301, 302, 303, 304, 305, 306, 307, 308
        , 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424, 425, 426, 428, 429, 431, 451
        , 500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511);
        if (in_array($code, $status_code)) {
            return $code;
        } else {
            return 500;
        }
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseServerError($codeText = 'internal server error occurred', $code = 'internal_server_error', $headers = []): JsonResponse
    {
        return $this->responseError(null, Response::HTTP_INTERNAL_SERVER_ERROR, $codeText, $code, $headers);
    }

    /**
     * @param $body
     * @param int $status
     * @param string $codeText
     * @param string $messageCode
     * @param array $headers
     * @return JsonResponse
     */
    public function responseError($body, $status = Response::HTTP_INTERNAL_SERVER_ERROR, $codeText = 'server error', $messageCode = 'server_error', $headers = []): JsonResponse
    {
        return $this->nicoResponse($body, $status, $codeText, $messageCode, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseUnAuthorize($codeText = 'unauthorized', $code = 'unauthorized', $headers = []): JsonResponse
    {
        return response()->json([
            'body' => null,
            'status' => [
                "message" => trans("appconstant." . $code),
                'code' => $code,
                'code_text' => $codeText,
            ]
        ], Response::HTTP_UNAUTHORIZED)->withHeaders($headers);
//        return $this->responseError(null, Response::HTTP_UNAUTHORIZED, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseForbidden($codeText = 'forbidden', $code = 'forbidden', $headers = []): JsonResponse
    {
        return $this->responseError(null, Response::HTTP_FORBIDDEN, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseNotFound($codeText = 'not found', $code = 'not_found', $headers = []): JsonResponse
    {
        return $this->responseError(null, Response::HTTP_NOT_FOUND, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseBadRequest($codeText = 'bad request', $code = 'bad_request', $headers = []): JsonResponse
    {
        return $this->responseError(null, Response::HTTP_BAD_REQUEST, $codeText, $code, $headers);
    }

    /**
     * @param string $body
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responsePreConditionFailed($body = '', $codeText = 'precondition failed', $code = 'precondition_failed', $headers = []): JsonResponse
    {
        return $this->responseError($body, Response::HTTP_PRECONDITION_FAILED, $codeText, $code, $headers);
    }

    /**
     * @param null $body
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseConflict($body = null, $codeText = 'conflict', $code = 'conflict', $headers = []): JsonResponse
    {
        return $this->responseError($body, Response::HTTP_CONFLICT, $codeText, $code, $headers);
    }

    /**
     * @param null $body
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseExpectationFailed($body = null, $codeText = 'expectation failed', $code = 'expectation_failed', $headers = []): JsonResponse
    {
        return $this->responseError($body, Response::HTTP_EXPECTATION_FAILED, $codeText, $code, $headers);
    }

    /**
     * @param null $body
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseValidationError($body = null, $codeText = 'form validation failed', $code = 'form_validation_error', $headers = []): JsonResponse
    {
        return $this->responseError($body, Response::HTTP_EXPECTATION_FAILED, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseTooManyAttempts($codeText = 'too many request', $code = 'too_many_request', $headers = []): JsonResponse
    {
        return $this->responseError(Null, Response::HTTP_TOO_MANY_REQUESTS, $codeText, $code, $headers);
    }

}
