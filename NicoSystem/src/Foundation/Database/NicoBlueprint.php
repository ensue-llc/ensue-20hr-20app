<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/22/2017
 * Time: 5:38 PM
 */

namespace NicoSystem\Foundation\Database;


use Illuminate\Database\Schema\Blueprint;

class NicoBlueprint extends Blueprint
{
    /**
     * The method that adds editors(created_by, updated_by and deleted_by) to the table
     */
    public function editors()
    {
        $this->creator();
        $this->updater();
        $this->deletor();
    }

    /**
     * @param string $columnName
     * @param string $foreignTable
     * @param string $foreignColumn
     */
    public function creator($columnName = "created_by", $foreignTable = "users", $foreignColumn = "id")
    {
        if (!$columnName) {
            $columnName = "created_by";
        }
        $this->editor($columnName, $foreignTable, $foreignColumn);
    }

    /**
     * @param $columnName
     * @param $foreignTable
     * @param $foreignColumn
     */
    protected function editor($columnName, $foreignTable, $foreignColumn)
    {
        $this->unsignedInteger($columnName)->nullable();
        if ($foreignTable && $foreignColumn) {
            $this->foreign($columnName)->references($foreignColumn)->on($foreignTable);
        }
    }

    /**
     * @param string $columnName
     * @param string $foreignTable
     * @param string $foreignColumn
     */
    public function updater($columnName = "updated_by", $foreignTable = "users", $foreignColumn = "id")
    {
        if (!$columnName) {
            $columnName = "created_by";
        }
        $this->editor($columnName, $foreignTable, $foreignColumn);
    }

    /**
     * @param string $columnName
     * @param string $foreignTable
     * @param string $foreignColumn
     */
    public function deletor($columnName = "deleted_by", $foreignTable = "users", $foreignColumn = "id")
    {
        if (!$columnName) {
            $columnName = "created_by";
        }
        $this->editor($columnName, $foreignTable, $foreignColumn);
    }

}
