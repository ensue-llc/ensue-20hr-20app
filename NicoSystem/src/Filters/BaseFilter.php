<?php
/**
 * Created by PhpStorm.
 * User: Rocco
 * Date: 10/25/2018
 * Time: 3:59 PM
 */

namespace NicoSystem\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class BaseFilter
 * @package NicoSystem\Filters
 */
abstract class BaseFilter
{
    /**
     * @var Builder
     */
    protected Builder $builder;

    /**
     * @var string
     */
    protected string $domainIdName = "site_id";

    /**
     * BaseFilter constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @param $filerOptions
     * @return Builder
     */
    public function attachFilterQuery($filerOptions): Builder
    {
        //creating the instance for filter
        foreach ($filerOptions as $key => $value) {

            if (method_exists($this, $key)) {
                call_user_func_array([$this, $key], array_filter([$value], 'strlen'));
            }
        }
        return $this->builder;
    }

    /**
     * @param string $title
     */
    public function title($title = ''): void
    {
        if ($title !== '') {
            $this->builder->where('title', 'like', "%{$title}%");
        }
    }

    /**
     * @param $keyword
     * @return mixed
     */
    public abstract function keyword($keyword): mixed;

    /**
     * @param $status
     */
    public function status($status = ''): void
    {
        if ($status != '' || $status != null) {
            $this->builder->where('status', $status);
        }
    }

}
