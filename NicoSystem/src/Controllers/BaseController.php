<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/30/2016
 * Time: 11:38 PM
 */

namespace NicoSystem\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use NicoSystem\Foundation\NicoResponseTraits;
use NicoSystem\Repositories\BaseRepository;
use NicoSystem\Requests\NicoRequest;

/**
 * Class BaseController
 * @package NicoSystem\Controllers
 */
abstract class BaseController extends Controller
{
    use NicoResponseTraits, AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var BaseRepository
     */
    protected BaseRepository $repository;

    /**
     * Toggle publish status of a resource
     * @param \NicoSystem\Requests\NicoRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function togglePublish(NicoRequest $request, int $id): JsonResponse
    {
        return $this->responseOk( $this->repository->togglePublish($id));
    }

}
